.data

_end: .asciiz "\n"
	.align 2
_buffer: .space 256
	.align 2
VReg: .space 3600
	length0: 	.word 	1
	String0: 	.asciiz 	" "
	length1: 	.word 	1
	String1: 	.asciiz 	"\n"


.text

# copy the string in $a0 to buffer in $a1, with putting '\0' in the end of the buffer
###### Checked ######
# used $v0, $a0, $a1
_string_copy:
	_begin_string_copy:
	lb $v0, 0($a0)
	beqz $v0, _exit_string_copy
	sb $v0, 0($a1)
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_string_copy
	_exit_string_copy:
	sb $zero, 0($a1)
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__print:
	li $v0, 4
	syscall
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__println:
	li $v0, 4
	syscall
	la $a0, _end
	syscall
	jr $ra

# count the length of given string in $a0
###### Checked ######
# used $v0, $v1, $a0
_count_string_length:
	move $v0, $a0

	_begin_count_string_length:
	lb $v1, 0($a0)
	beqz $v1, _exit_count_string_length
	add $a0, $a0, 1
	j _begin_count_string_length

	_exit_count_string_length:
	sub $v0, $a0, $v0
	jr $ra

# non arg, string in $v0
###### Checked ######
# used $a0, $a1, $t0, $v0, (used in _count_string_length) $v1
func__getString:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	la $a0, _buffer
	li $a1, 255
	li $v0, 8
	syscall

	jal _count_string_length

	move $a1, $v0			# now $a1 contains the length of the string
	add $a0, $v0, 5			# total required space = length + 1('\0') + 1 word(record the length of the string)
	li $v0, 9
	syscall
	sw $a1, 0($v0)
	add $v0, $v0, 4
	la $a0, _buffer
	move $a1, $v0
	move $t0, $v0
	jal _string_copy
	move $v0, $t0

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# non arg, int in $v0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__getInt:
	li $v0, 5
	syscall
	jr $ra

# int arg in $a0
###### Checked ######
# Bug fixed(5/2): when the arg is a neg number
# Change(5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__toString:
	subu $sp, $sp, 24
	sw $a0, 0($sp)
	sw $t0, 4($sp)
	sw $t1, 8($sp)
	sw $t2, 12($sp)
	sw $t3, 16($sp)
	sw $t5, 20($sp)

	# first count the #digits
	li $t0, 0			# $t0 = 0 if the number is a negnum
	bgez $a0, _skip_set_less_than_zero
	li $t0, 1			# now $t0 must be 1
	neg $a0, $a0
	_skip_set_less_than_zero:
	beqz $a0, _set_zero

	li $t1, 0			# the #digits is in $t1
	move $t2, $a0
	move $t3, $a0
	li $t5, 10

	_begin_count_digit:
	div $t2, $t5
	mflo $v0			# get the quotient
	mfhi $v1			# get the remainder
	bgtz $v0 _not_yet
	bgtz $v1 _not_yet
	j _yet
	_not_yet:
	add $t1, $t1, 1
	move $t2, $v0
	j _begin_count_digit

	_yet:
	beqz $t0, _skip_reserve_neg
	add $t1, $t1, 1
	_skip_reserve_neg:
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v0, $v0, 4
	add $t1, $t1, $v0
	sb $zero, 0($t1)
	sub $t1, $t1, 1

	_continue_toString:
	div $t3, $t5
	mfhi $v1
	add $v1, $v1, 48	# in ascii 48 = '0'
	sb $v1, 0($t1)
	sub $t1, $t1, 1
	mflo $t3
	# bge $t1, $v0, _continue_toString
	bnez $t3, _continue_toString

	beqz $t0, _skip_place_neg
	li $v1, 45
	sb $v1, 0($t1)
	_skip_place_neg:
	# lw $ra, 0($sp)
	# addu $sp, $sp, 4

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra

	_set_zero:
	li $a0, 6
	li $v0, 9
	syscall
	li $a0, 1
	sw $a0, 0($v0)
	add $v0, $v0, 4
	li $a0, 48
	sb $a0, 0($v0)

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra


# string arg in $v0
# the zero in the end of the string will not be counted
###### Checked ######
# you don't need to preserve reg before calling it
func__string.length:
	lw $v0, -4($v0)
	jr $ra

# string arg in $a0, left in $a1, right in $a2
###### Checked ######
# used $a0, $a1, $t0, $t1, $t2, $v1, $v0
func__string.substring:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	move $t0, $v0
	move $t3, $a0
	sub $t1, $a1, $a0
	add $t1, $t1, 1		# $t1 is the length of the substring
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v1, $v0, 4

	add $a0, $t0, $t3
	add $t2, $t0, $a1
	lb $t1, 1($t2)		# store the ori_begin + right + 1 char in $t1
	sb $zero, 1($t2)	# change it to 0 for the convenience of copying
	move $a1, $v1
	jal _string_copy
	move $v0, $v1
	sb $t1, 1($t2)

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra
# string arg in
###### Checked ######
# 16/5/4 Fixed a serious bug: can not parse negtive number
# used $v0, $v1
func__string.parseInt:
	move $a0, $v0
	li $v0, 0

	lb $t1, 0($a0)
	li $t2, 45
	bne $t1, $t2, _skip_parse_neg
	li $t1, 1			#if there is a '-' sign, $t1 = 1
	add $a0, $a0, 1
	j _skip_set_t1_zero

	_skip_parse_neg:
	li $t1, 0
	_skip_set_t1_zero:
	move $t0, $a0
	li $t2, 1

	_count_number_pos:
	lb $v1, 0($t0)
	bgt $v1, 57, _begin_parse_int
	blt $v1, 48, _begin_parse_int
	add $t0, $t0, 1
	j _count_number_pos

	_begin_parse_int:
	sub $t0, $t0, 1

	_parsing_int:
	blt $t0, $a0, _finish_parse_int
	lb $v1, 0($t0)
	sub $v1, $v1, 48
	mul $v1, $v1, $t2
	add $v0, $v0, $v1
	mul $t2, $t2, 10
	sub $t0, $t0, 1
	j _parsing_int

	_finish_parse_int:
	beqz $t1, _skip_neg
	neg $v0, $v0
	_skip_neg:

	jr $ra

# string arg in $a0, pos in $a1
###### Checked ######
# used $v0, $v1
func__string.ord:
	add $v0, $v0, $a0
	lb $v0, 0($v0)
	jr $ra

# array arg in $a0
# used $v0
func__array.size:
	lw $v0, -4($v0)
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__stringConcatenate:

	subu $sp, $sp, 24
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $t0, 12($sp)
	sw $t1, 16($sp)
	sw $t2, 20($sp)

	lw $t0, -4($a0)		# $t0 is the length of lhs
	lw $t1, -4($a1)		# $t1 is the length of rhs
	add $t2, $t0, $t1

	move $t1, $a0

	add $a0, $t2, 5
	li $v0, 9
	syscall

	sw $t2, 0($v0)
	move $t2, $a1

	add $v0, $v0, 4
	move $v1, $v0

	move $a0, $t1
	move $a1, $v1
	jal _string_copy

	move $a0, $t2
	add $a1, $v1, $t0
	# add $a1, $a1, 1
	jal _string_copy

	move $v0, $v1
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $t0, 12($sp)
	lw $t1, 16($sp)
	lw $t2, 20($sp)
	addu $sp, $sp, 24
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringIsEqual:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	lw $v0, -4($a0)
	lw $v1, -4($a1)
	bne $v0, $v1, _not_equal

	_continue_compare_equal:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	beqz $v0, _equal
	bne $v0, $v1, _not_equal
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _continue_compare_equal

	_not_equal:
	li $v0, 0
	j _compare_final

	_equal:
	li $v0, 1

	_compare_final:
	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra


# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringLess:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	_begin_compare_less:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	blt $v0, $v1, _less_correct
	bgt $v0, $v1, _less_false
	beqz $v0, _less_false
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_compare_less

	_less_correct:
	li $v0, 1
	j _less_compare_final

	_less_false:
	li $v0, 0

	_less_compare_final:

	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLarge:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringLess

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	bnez $v0, _skip_compare_equal_in_Leq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual

	_skip_compare_equal_in_Leq:
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringGeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	beqz $v0, _skip_compare_equal_in_Geq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual
	xor $v0, $v0, 1

	_skip_compare_equal_in_Geq:
	xor $v0, $v0, 1
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringNeq:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringIsEqual

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

main:
	sub $29 $29 4
	li $4 4
	li $2 9
	syscall
	li $4 8
	li $2 9
	syscall
	move $23 $2
	jal main_0
	li $2 10
	syscall

main_0:
	move $31 $31
	sw $31 0($29)
	jal func__getString
	move $2 $2
	jal func__string.parseInt
	move $24 $2
	lw $25 0($23)
	la $30 0($23)
	move $25 $24
	sw $25 0($23)
	lw $t1 0($23)
	li $4 4
	li $2 9
	syscall
	sw $t1 0($2)
	mul $4 $t1 8
	li $2 9
	syscall
	move $10 $2
	lw $11 4($23)
	la $30 4($23)
	move $11 $10
	sw $11 4($23)
	li $s0 0
	lw $12 4($23)
	move $2 $12
	jal func__array.size
	move $13 $2
	bge $s0 $13 main_0_afterLoop

main_0_loop:
	lw $12 4($23)
	mul $14 $s0 4
	add $15 $12 $14
	lw $7 0($15)
	la $30 0($15)
	move $7 $s0
	sw $7 0($30)

main_0_loopTail:
	add $6 $s0 1
	move $s0 $6
	lw $12 4($23)
	move $2 $12
	jal func__array.size
	move $24 $2
	blt $s0 $24 main_0_loop

main_0_afterLoop:
	jal func__makeHeap
	jal func__heapSort
	li $s0 0
	lw $12 4($23)
	move $2 $12
	jal func__array.size
	move $25 $2
	bge $s0 $25 main_0_afterLoop_afterLoop

main_0_afterLoop_loop:
	lw $12 4($23)
	mul $10 $s0 4
	add $11 $12 $10
	lw $13 0($11)
	la $30 0($11)
	move $4 $13
	jal func__toString
	move $4 $2
	la $5 String0
	jal func__stringConcatenate
	move $14 $2
	move $4 $14
	jal func__print

main_0_afterLoop_loopTail:
	add $15 $s0 1
	move $s0 $15
	lw $12 4($23)
	move $2 $12
	jal func__array.size
	move $7 $2
	blt $s0 $7 main_0_afterLoop_loop

main_0_afterLoop_afterLoop:
	la $4 String1
	jal func__print
	li $2 0
	lw $3 0($29)
	move $31 $3
	add $29 $29 4
	jr $ra
	lw $3 0($29)
	move $31 $3
	add $29 $29 4
	jr $ra

func__exchange:
	move $24 $31
	move $25 $4
	move $9 $5
	lw $10 4($23)
	mul $11 $25 4
	add $12 $10 $11
	lw $13 0($12)
	la $30 0($12)
	move $14 $13
	lw $10 4($23)
	mul $15 $9 4
	add $7 $10 $15
	lw $6 0($7)
	la $30 0($7)
	lw $10 4($23)
	mul $11 $25 4
	add $12 $10 $11
	lw $13 0($12)
	la $30 0($12)
	move $13 $6
	sw $13 0($30)
	lw $10 4($23)
	mul $15 $9 4
	add $7 $10 $15
	lw $25 0($7)
	la $30 0($7)
	move $25 $14
	sw $25 0($30)
	move $31 $24
	jr $ra

func__makeHeap:
	move $s3 $31
	lw $t1 0($23)
	sub $24 $t1 1
	div $25 $24 2
	move $s1 $25
	li $t2 0
	blt $s1 0 func__makeHeap_afterLoop

func__makeHeap_loop:
	mul $11 $s1 2
	move $t2 $11
	mul $12 $s1 2
	add $13 $12 1
	lw $t1 0($23)
	bge $13 $t1 func__makeHeap_loop_shortcut

func__makeHeap_loop_normal:
	lw $14 4($23)
	mul $15 $s1 2
	add $7 $15 1
	mul $6 $7 4
	add $24 $14 $6
	lw $25 0($24)
	la $30 0($24)
	lw $14 4($23)
	mul $11 $s1 2
	mul $12 $11 4
	add $13 $14 $12
	lw $15 0($13)
	la $30 0($13)
	blt $25 $15 func__makeHeap_loop_normalEnd

func__makeHeap_loop_shortcut:
	li $s2 0
	b func__makeHeap_loop_next

func__makeHeap_loop_normalEnd:
	li $s2 1

func__makeHeap_loop_next:
	bne $s2 0 func__makeHeap_loop_next_branch_then

func__makeHeap_loop_next_branch_else:
	b func__makeHeap_loop_next_afterBranch

func__makeHeap_loop_next_branch_then:
	mul $7 $s1 2
	add $6 $7 1
	move $t2 $6

func__makeHeap_loop_next_afterBranch:
	lw $14 4($23)
	mul $24 $s1 4
	add $11 $14 $24
	lw $12 0($11)
	la $30 0($11)
	lw $14 4($23)
	mul $13 $t2 4
	add $25 $14 $13
	lw $15 0($25)
	la $30 0($25)
	bgt $12 $15 func__makeHeap_loop_next_afterBranch_branch_then

func__makeHeap_loop_next_afterBranch_branch_else:
	b func__makeHeap_loop_next_afterBranch_afterBranch

func__makeHeap_loop_next_afterBranch_branch_then:
	move $4 $s1
	move $5 $t2
	jal func__exchange

func__makeHeap_loop_next_afterBranch_afterBranch:
	sub $7 $s1 1
	move $s1 $7

func__makeHeap_loopTail:
	bge $s1 0 func__makeHeap_loop

func__makeHeap_afterLoop:
	li $2 0
	move $31 $s3
	jr $ra
	move $31 $s3
	jr $ra

func__adjustHeap:
	move $t4 $31
	move $t3 $4
	li $24 0
	move $t1 $24
	move $t2 $t1
	mul $25 $t2 2
	bge $25 $t3 func__adjustHeap_afterLoop

func__adjustHeap_loop:
	mul $13 $t2 2
	move $t1 $13
	mul $14 $t2 2
	add $15 $14 1
	bge $15 $t3 func__adjustHeap_loop_shortcut

func__adjustHeap_loop_normal:
	lw $7 4($23)
	mul $6 $t2 2
	add $24 $6 1
	mul $25 $24 4
	add $13 $7 $25
	lw $14 0($13)
	la $30 0($13)
	lw $7 4($23)
	mul $15 $t2 2
	mul $6 $15 4
	add $24 $7 $6
	lw $25 0($24)
	la $30 0($24)
	blt $14 $25 func__adjustHeap_loop_normalEnd

func__adjustHeap_loop_shortcut:
	li $s4 0
	b func__adjustHeap_loop_next

func__adjustHeap_loop_normalEnd:
	li $s4 1

func__adjustHeap_loop_next:
	bne $s4 0 func__adjustHeap_loop_next_branch_then

func__adjustHeap_loop_next_branch_else:
	b func__adjustHeap_loop_next_afterBranch

func__adjustHeap_loop_next_branch_then:
	mul $13 $t2 2
	add $15 $13 1
	move $t1 $15

func__adjustHeap_loop_next_afterBranch:
	lw $7 4($23)
	mul $6 $t2 4
	add $24 $7 $6
	lw $14 0($24)
	la $30 0($24)
	lw $7 4($23)
	mul $25 $t1 4
	add $13 $7 $25
	lw $15 0($13)
	la $30 0($13)
	bgt $14 $15 func__adjustHeap_loop_next_afterBranch_branch_then

func__adjustHeap_loop_next_afterBranch_branch_else:
	b func__adjustHeap_afterLoop
	b func__adjustHeap_loop_next_afterBranch_afterBranch

func__adjustHeap_loop_next_afterBranch_branch_then:
	lw $7 4($23)
	mul $6 $t2 4
	add $24 $7 $6
	lw $25 0($24)
	la $30 0($24)
	move $13 $25
	lw $7 4($23)
	mul $14 $t1 4
	add $15 $7 $14
	lw $6 0($15)
	la $30 0($15)
	lw $7 4($23)
	mul $24 $t2 4
	add $25 $7 $24
	lw $14 0($25)
	la $30 0($25)
	move $14 $6
	sw $14 0($30)
	lw $7 4($23)
	mul $15 $t1 4
	add $24 $7 $15
	lw $25 0($24)
	la $30 0($24)
	move $25 $13
	sw $25 0($30)
	move $t2 $t1

func__adjustHeap_loop_next_afterBranch_afterBranch:

func__adjustHeap_loopTail:
	mul $6 $t2 2
	blt $6 $t3 func__adjustHeap_loop

func__adjustHeap_afterLoop:
	li $2 0
	move $31 $t4
	jr $ra
	move $31 $t4
	jr $ra

func__heapSort:
	move $s6 $31
	li $t2 0
	li $s5 0
	lw $t1 0($23)
	bge $s5 $t1 func__heapSort_afterLoop

func__heapSort_loop:
	lw $24 4($23)
	lw $25 0($24)
	la $30 0($24)
	move $t2 $25
	lw $24 4($23)
	lw $t1 0($23)
	sub $11 $t1 $s5
	sub $12 $11 1
	mul $13 $12 4
	add $14 $24 $13
	lw $15 0($14)
	la $30 0($14)
	lw $24 4($23)
	lw $7 0($24)
	la $30 0($24)
	move $7 $15
	sw $7 0($30)
	lw $24 4($23)
	lw $t1 0($23)
	sub $6 $t1 $s5
	sub $25 $6 1
	mul $11 $25 4
	add $12 $24 $11
	lw $13 0($12)
	la $30 0($12)
	move $13 $t2
	sw $13 0($30)
	lw $t1 0($23)
	sub $14 $t1 $s5
	sub $15 $14 1
	move $4 $15
	jal func__adjustHeap

func__heapSort_loopTail:
	add $7 $s5 1
	move $s5 $7
	lw $t1 0($23)
	blt $s5 $t1 func__heapSort_loop

func__heapSort_afterLoop:
	li $2 0
	move $31 $s6
	jr $ra
	move $31 $s6
	jr $ra

