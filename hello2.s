.data

_end: .asciiz "\n"
	.align 2
_buffer: .space 256
	.align 2
VReg: .space 3600
	length0: 	.word 	1
	String0: 	.asciiz 	" "
	length1: 	.word 	1
	String1: 	.asciiz 	"\n"


.text
# copy the string in $a0 to buffer in $a1, with putting '\0' in the end of the buffer
###### Checked ######
# used $v0, $a0, $a1
_string_copy:
	_begin_string_copy:
	lb $v0, 0($a0)
	beqz $v0, _exit_string_copy
	sb $v0, 0($a1)
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_string_copy
	_exit_string_copy:
	sb $zero, 0($a1)
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__print:
	li $v0, 4
	syscall
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__println:
	li $v0, 4
	syscall
	la $a0, _end
	syscall
	jr $ra

# count the length of given string in $a0
###### Checked ######
# used $v0, $v1, $a0
_count_string_length:
	move $v0, $a0

	_begin_count_string_length:
	lb $v1, 0($a0)
	beqz $v1, _exit_count_string_length
	add $a0, $a0, 1
	j _begin_count_string_length

	_exit_count_string_length:
	sub $v0, $a0, $v0
	jr $ra

# non arg, string in $v0
###### Checked ######
# used $a0, $a1, $t0, $v0, (used in _count_string_length) $v1
func__getString:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	la $a0, _buffer
	li $a1, 255
	li $v0, 8
	syscall

	jal _count_string_length

	move $a1, $v0			# now $a1 contains the length of the string
	add $a0, $v0, 5			# total required space = length + 1('\0') + 1 word(record the length of the string)
	li $v0, 9
	syscall
	sw $a1, 0($v0)
	add $v0, $v0, 4
	la $a0, _buffer
	move $a1, $v0
	move $t0, $v0
	jal _string_copy
	move $v0, $t0

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# non arg, int in $v0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__getInt:
	li $v0, 5
	syscall
	jr $ra

# int arg in $a0
###### Checked ######
# Bug fixed(5/2): when the arg is a neg number
# Change(5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__toString:
	subu $sp, $sp, 24
	sw $a0, 0($sp)
	sw $t0, 4($sp)
	sw $t1, 8($sp)
	sw $t2, 12($sp)
	sw $t3, 16($sp)
	sw $t5, 20($sp)

	# first count the #digits
	li $t0, 0			# $t0 = 0 if the number is a negnum
	bgez $a0, _skip_set_less_than_zero
	li $t0, 1			# now $t0 must be 1
	neg $a0, $a0
	_skip_set_less_than_zero:
	beqz $a0, _set_zero

	li $t1, 0			# the #digits is in $t1
	move $t2, $a0
	move $t3, $a0
	li $t5, 10

	_begin_count_digit:
	div $t2, $t5
	mflo $v0			# get the quotient
	mfhi $v1			# get the remainder
	bgtz $v0 _not_yet
	bgtz $v1 _not_yet
	j _yet
	_not_yet:
	add $t1, $t1, 1
	move $t2, $v0
	j _begin_count_digit

	_yet:
	beqz $t0, _skip_reserve_neg
	add $t1, $t1, 1
	_skip_reserve_neg:
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v0, $v0, 4
	add $t1, $t1, $v0
	sb $zero, 0($t1)
	sub $t1, $t1, 1

	_continue_toString:
	div $t3, $t5
	mfhi $v1
	add $v1, $v1, 48	# in ascii 48 = '0'
	sb $v1, 0($t1)
	sub $t1, $t1, 1
	mflo $t3
	# bge $t1, $v0, _continue_toString
	bnez $t3, _continue_toString

	beqz $t0, _skip_place_neg
	li $v1, 45
	sb $v1, 0($t1)
	_skip_place_neg:
	# lw $ra, 0($sp)
	# addu $sp, $sp, 4

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra

	_set_zero:
	li $a0, 6
	li $v0, 9
	syscall
	li $a0, 1
	sw $a0, 0($v0)
	add $v0, $v0, 4
	li $a0, 48
	sb $a0, 0($v0)

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra


# string arg in $v0
# the zero in the end of the string will not be counted
###### Checked ######
# you don't need to preserve reg before calling it
func__string.length:
	lw $v0, -4($v0)
	jr $ra

# string arg in $a0, left in $a1, right in $a2
###### Checked ######
# used $a0, $a1, $t0, $t1, $t2, $v1, $v0
func__string.substring:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	move $t0, $v0
	move $t3, $a0
	sub $t1, $a1, $a0
	add $t1, $t1, 1		# $t1 is the length of the substring
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v1, $v0, 4

	add $a0, $t0, $t3
	add $t2, $t0, $a1
	lb $t1, 1($t2)		# store the ori_begin + right + 1 char in $t1
	sb $zero, 1($t2)	# change it to 0 for the convenience of copying
	move $a1, $v1
	jal _string_copy
	move $v0, $v1
	sb $t1, 1($t2)

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra
# string arg in
###### Checked ######
# 16/5/4 Fixed a serious bug: can not parse negtive number
# used $v0, $v1
func__string.parseInt:
	move $a0, $v0
	li $v0, 0

	lb $t1, 0($a0)
	li $t2, 45
	bne $t1, $t2, _skip_parse_neg
	li $t1, 1			#if there is a '-' sign, $t1 = 1
	add $a0, $a0, 1
	j _skip_set_t1_zero

	_skip_parse_neg:
	li $t1, 0
	_skip_set_t1_zero:
	move $t0, $a0
	li $t2, 1

	_count_number_pos:
	lb $v1, 0($t0)
	bgt $v1, 57, _begin_parse_int
	blt $v1, 48, _begin_parse_int
	add $t0, $t0, 1
	j _count_number_pos

	_begin_parse_int:
	sub $t0, $t0, 1

	_parsing_int:
	blt $t0, $a0, _finish_parse_int
	lb $v1, 0($t0)
	sub $v1, $v1, 48
	mul $v1, $v1, $t2
	add $v0, $v0, $v1
	mul $t2, $t2, 10
	sub $t0, $t0, 1
	j _parsing_int

	_finish_parse_int:
	beqz $t1, _skip_neg
	neg $v0, $v0
	_skip_neg:

	jr $ra

# string arg in $a0, pos in $a1
###### Checked ######
# used $v0, $v1
func__string.ord:
	add $v0, $v0, $a0
	lb $v0, 0($v0)
	jr $ra

# array arg in $a0
# used $v0
func__array.size:
	lw $v0, -4($v0)
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__stringConcatenate:

	subu $sp, $sp, 24
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $t0, 12($sp)
	sw $t1, 16($sp)
	sw $t2, 20($sp)

	lw $t0, -4($a0)		# $t0 is the length of lhs
	lw $t1, -4($a1)		# $t1 is the length of rhs
	add $t2, $t0, $t1

	move $t1, $a0

	add $a0, $t2, 5
	li $v0, 9
	syscall

	sw $t2, 0($v0)
	move $t2, $a1

	add $v0, $v0, 4
	move $v1, $v0

	move $a0, $t1
	move $a1, $v1
	jal _string_copy

	move $a0, $t2
	add $a1, $v1, $t0
	# add $a1, $a1, 1
	jal _string_copy

	move $v0, $v1
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $t0, 12($sp)
	lw $t1, 16($sp)
	lw $t2, 20($sp)
	addu $sp, $sp, 24
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringIsEqual:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	lw $v0, -4($a0)
	lw $v1, -4($a1)
	bne $v0, $v1, _not_equal

	_continue_compare_equal:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	beqz $v0, _equal
	bne $v0, $v1, _not_equal
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _continue_compare_equal

	_not_equal:
	li $v0, 0
	j _compare_final

	_equal:
	li $v0, 1

	_compare_final:
	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra


# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringLess:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	_begin_compare_less:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	blt $v0, $v1, _less_correct
	bgt $v0, $v1, _less_false
	beqz $v0, _less_false
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_compare_less

	_less_correct:
	li $v0, 1
	j _less_compare_final

	_less_false:
	li $v0, 0

	_less_compare_final:

	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLarge:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringLess

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	bnez $v0, _skip_compare_equal_in_Leq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual

	_skip_compare_equal_in_Leq:
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringGeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	beqz $v0, _skip_compare_equal_in_Geq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual
	xor $v0, $v0, 1

	_skip_compare_equal_in_Geq:
	xor $v0, $v0, 1
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringNeq:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringIsEqual

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# Read: 2
# Write: 4 2 23
main:
	sub $29 $29 4
	li $4 4
	li $2 9
	syscall
	li $4 8
	li $2 9
	syscall
	move $23 $2
	jal main_0
	li $2 10
	syscall

# Read: 31 2 23 33 34 35 36 37 38 40 41
# Write: 32 2 33 34 30 35 4 36 37 40 38 41
main_0:
	move $32 $31
	sw $31 0($29)
	jal func__getString
	move $2 $2
	jal func__string.parseInt
	move $33 $2
	lw $34 0($23)
	la $30 0($23)
	move $34 $33
	sw $34 0($23)
	lw $35 0($23)
	li $4 4
	li $2 9
	syscall
	sw $35 0($2)
	mul $4 $35 8
	li $2 9
	syscall
	move $36 $2
	lw $37 4($23)
	la $30 4($23)
	move $37 $36
	sw $37 4($23)
	li $40 0
	lw $38 4($23)
	move $2 $38
	jal func__array.size
	move $41 $2
	bge $40 $41 main_0_afterLoop

# Read: 23 40 38 44 45 30 43
# Write: 38 44 45 43 30
main_0_loop:
	lw $38 4($23)
	mul $44 $40 4
	add $45 $38 $44
	lw $43 0($45)
	la $30 0($45)
	move $43 $40
	sw $43 0($30)

# Read: 40 46 23 38 2 47
# Write: 46 40 38 2 47
main_0_loopTail:
	add $46 $40 1
	move $40 $46
	lw $38 4($23)
	move $2 $38
	jal func__array.size
	move $47 $2
	blt $40 $47 main_0_loop

# Read: 23 38 2 40 49
# Write: 40 38 2 49
main_0_afterLoop:
	jal func__makeHeap
	jal func__heapSort
	li $40 0
	lw $38 4($23)
	move $2 $38
	jal func__array.size
	move $49 $2
	bge $40 $49 main_0_afterLoop_afterLoop

# Read: 23 40 38 52 53 51 2 54
# Write: 38 52 53 51 30 4 5 54
main_0_afterLoop_loop:
	lw $38 4($23)
	mul $52 $40 4
	add $53 $38 $52
	lw $51 0($53)
	la $30 0($53)
	move $4 $51
	jal func__toString
	move $4 $2
	la $5 String0
	jal func__stringConcatenate
	move $54 $2
	move $4 $54
	jal func__print

# Read: 40 55 23 38 2 56
# Write: 55 40 38 2 56
main_0_afterLoop_loopTail:
	add $55 $40 1
	move $40 $55
	lw $38 4($23)
	move $2 $38
	jal func__array.size
	move $56 $2
	blt $40 $56 main_0_afterLoop_loop

# Read: 32
# Write: 4 2 31
main_0_afterLoop_afterLoop:
	la $4 String1
	jal func__print
	li $2 0
	lw $3 0($29)
	move $31 $32
	add $29 $29 4
	jr $ra
	lw $3 0($29)
	move $31 $32
	add $29 $29 4
	jr $ra

# local: 2 31 23 33 34 36 37 38 41 44 45 30 43 46 47 49 52 53 51 54 55 56
# localSaved: 35
# global: 40
# Save in address: 32
# times: $4: 8  $2: 22  $23: 14  $31: 3  $32: 3  $33: 2  $34: 3  $30: 5  $35: 3  $36: 2  $37: 3  $40: 13  $38: 12  $41: 2  $44: 2  $45: 3  $43: 3  $46: 2  $47: 2  $49: 2  $52: 2  $53: 3  $51: 2  $5: 1  $54: 2  $55: 2  $56: 2 
# Read: 31 4 5 23 59 38 62 63 61 60 67 68 70 71 66 30 69 73 74 65 72 58
# Write: 58 59 60 38 62 63 61 30 65 67 68 66 70 71 69 73 74 72 31
func__exchange:
	move $58 $31
	move $59 $4
	move $60 $5
	lw $38 4($23)
	mul $62 $59 4
	add $63 $38 $62
	lw $61 0($63)
	la $30 0($63)
	move $65 $61
	lw $38 4($23)
	mul $67 $60 4
	add $68 $38 $67
	lw $66 0($68)
	la $30 0($68)
	lw $38 4($23)
	mul $70 $59 4
	add $71 $38 $70
	lw $69 0($71)
	la $30 0($71)
	move $69 $66
	sw $69 0($30)
	lw $38 4($23)
	mul $73 $60 4
	add $74 $38 $73
	lw $72 0($74)
	la $30 0($74)
	move $72 $65
	sw $72 0($30)
	move $31 $58
	jr $ra

# local: 31 4 5 23 59 38 62 63 61 60 67 68 70 71 66 30 69 73 74 65 72 58
# localSaved:
# global:
# Save in address:
# times: $31: 2  $58: 2  $4: 1  $59: 3  $5: 1  $60: 3  $23: 4  $38: 8  $62: 2  $63: 3  $61: 2  $30: 6  $65: 2  $67: 2  $68: 3  $66: 2  $70: 2  $71: 3  $69: 3  $73: 2  $74: 3  $72: 3 
# Read: 31 23 35 76 77 81 79
# Write: 75 35 76 77 79 81 83
func__makeHeap:
	move $75 $31
	lw $35 0($23)
	sub $76 $35 1
	div $77 $76 2
	move $79 $77
	li $81 0
	blt $79 0 func__makeHeap_afterLoop

# Read: 79 85 86 23 87 35
# Write: 85 81 86 87 35
func__makeHeap_loop:
	mul $85 $79 2
	move $81 $85
	mul $86 $79 2
	add $87 $86 1
	lw $35 0($23)
	bge $87 $35 func__makeHeap_loop_shortcut

# Read: 23 79 90 91 38 93 94 95 97 98 92 96
# Write: 38 90 91 93 94 92 30 95 97 98 96
func__makeHeap_loop_normal:
	lw $38 4($23)
	mul $90 $79 2
	add $91 $90 1
	mul $93 $91 4
	add $94 $38 $93
	lw $92 0($94)
	la $30 0($94)
	lw $38 4($23)
	mul $95 $79 2
	mul $97 $95 4
	add $98 $38 $97
	lw $96 0($98)
	la $30 0($98)
	blt $92 $96 func__makeHeap_loop_normalEnd

# Read:
# Write: 89
func__makeHeap_loop_shortcut:
	li $89 0
	b func__makeHeap_loop_next

# Read:
# Write: 89
func__makeHeap_loop_normalEnd:
	li $89 1

# Read: 89
# Write:
func__makeHeap_loop_next:
	bne $89 0 func__makeHeap_loop_next_branch_then

# Read:
# Write:
func__makeHeap_loop_next_branch_else:
	b func__makeHeap_loop_next_afterBranch

# Read: 79 100 101
# Write: 100 101 81
func__makeHeap_loop_next_branch_then:
	mul $100 $79 2
	add $101 $100 1
	move $81 $101

# Read: 23 79 38 103 104 81 106 107 102 105
# Write: 38 103 104 102 30 106 107 105
func__makeHeap_loop_next_afterBranch:
	lw $38 4($23)
	mul $103 $79 4
	add $104 $38 $103
	lw $102 0($104)
	la $30 0($104)
	lw $38 4($23)
	mul $106 $81 4
	add $107 $38 $106
	lw $105 0($107)
	la $30 0($107)
	bgt $102 $105 func__makeHeap_loop_next_afterBranch_branch_then

# Read:
# Write:
func__makeHeap_loop_next_afterBranch_branch_else:
	b func__makeHeap_loop_next_afterBranch_afterBranch

# Read: 79 81
# Write: 4 5
func__makeHeap_loop_next_afterBranch_branch_then:
	move $4 $79
	move $5 $81
	jal func__exchange

# Read: 79 109
# Write: 109 79
func__makeHeap_loop_next_afterBranch_afterBranch:
	sub $109 $79 1
	move $79 $109

# Read: 79
# Write:
func__makeHeap_loopTail:
	bge $79 0 func__makeHeap_loop

# Read: 75
# Write: 2 31
func__makeHeap_afterLoop:
	li $2 0
	move $31 $75
	jr $ra
	move $31 $75
	jr $ra

# local: 31 23 76 77 85 86 87 90 91 38 93 94 95 97 98 92 96 100 101 103 104 106 107 102 105 109
# localSaved: 35 81
# global: 79 89 75
# Save in address:
# times: $31: 3  $75: 3  $23: 6  $35: 4  $76: 2  $77: 2  $79: 12  $81: 6  $83: 1  $85: 2  $86: 2  $87: 2  $38: 8  $90: 2  $91: 2  $93: 2  $94: 3  $92: 2  $30: 4  $95: 2  $97: 2  $98: 3  $96: 2  $89: 3  $100: 2  $101: 2  $103: 2  $104: 3  $102: 2  $106: 2  $107: 3  $105: 2  $4: 1  $5: 1  $109: 2  $2: 1 
# Read: 31 4 114 116 118 119 112
# Write: 111 112 114 116 118 119
func__adjustHeap:
	move $111 $31
	move $112 $4
	li $114 0
	move $116 $114
	move $118 $116
	mul $119 $118 2
	bge $119 $112 func__adjustHeap_afterLoop

# Read: 118 121 122 123 112
# Write: 121 116 122 123
func__adjustHeap_loop:
	mul $121 $118 2
	move $116 $121
	mul $122 $118 2
	add $123 $122 1
	bge $123 $112 func__adjustHeap_loop_shortcut

# Read: 23 118 126 127 38 129 130 131 133 134 128 132
# Write: 38 126 127 129 130 128 30 131 133 134 132
func__adjustHeap_loop_normal:
	lw $38 4($23)
	mul $126 $118 2
	add $127 $126 1
	mul $129 $127 4
	add $130 $38 $129
	lw $128 0($130)
	la $30 0($130)
	lw $38 4($23)
	mul $131 $118 2
	mul $133 $131 4
	add $134 $38 $133
	lw $132 0($134)
	la $30 0($134)
	blt $128 $132 func__adjustHeap_loop_normalEnd

# Read:
# Write: 125
func__adjustHeap_loop_shortcut:
	li $125 0
	b func__adjustHeap_loop_next

# Read:
# Write: 125
func__adjustHeap_loop_normalEnd:
	li $125 1

# Read: 125
# Write:
func__adjustHeap_loop_next:
	bne $125 0 func__adjustHeap_loop_next_branch_then

# Read:
# Write:
func__adjustHeap_loop_next_branch_else:
	b func__adjustHeap_loop_next_afterBranch

# Read: 118 136 137
# Write: 136 137 116
func__adjustHeap_loop_next_branch_then:
	mul $136 $118 2
	add $137 $136 1
	move $116 $137

# Read: 23 118 38 139 140 116 142 143 138 141
# Write: 38 139 140 138 30 142 143 141
func__adjustHeap_loop_next_afterBranch:
	lw $38 4($23)
	mul $139 $118 4
	add $140 $38 $139
	lw $138 0($140)
	la $30 0($140)
	lw $38 4($23)
	mul $142 $116 4
	add $143 $38 $142
	lw $141 0($143)
	la $30 0($143)
	bgt $138 $141 func__adjustHeap_loop_next_afterBranch_branch_then

# Read:
# Write:
func__adjustHeap_loop_next_afterBranch_branch_else:
	b func__adjustHeap_afterLoop
	b func__adjustHeap_loop_next_afterBranch_afterBranch

# Read: 23 118 38 146 147 145 116 151 152 154 155 150 30 153 157 158 149 156
# Write: 38 146 147 145 30 149 151 152 150 154 155 153 157 158 156 118
func__adjustHeap_loop_next_afterBranch_branch_then:
	lw $38 4($23)
	mul $146 $118 4
	add $147 $38 $146
	lw $145 0($147)
	la $30 0($147)
	move $149 $145
	lw $38 4($23)
	mul $151 $116 4
	add $152 $38 $151
	lw $150 0($152)
	la $30 0($152)
	lw $38 4($23)
	mul $154 $118 4
	add $155 $38 $154
	lw $153 0($155)
	la $30 0($155)
	move $153 $150
	sw $153 0($30)
	lw $38 4($23)
	mul $157 $116 4
	add $158 $38 $157
	lw $156 0($158)
	la $30 0($158)
	move $156 $149
	sw $156 0($30)
	move $118 $116

# Read:
# Write:
func__adjustHeap_loop_next_afterBranch_afterBranch:

# Read: 118 159 112
# Write: 159
func__adjustHeap_loopTail:
	mul $159 $118 2
	blt $159 $112 func__adjustHeap_loop

# Read: 111
# Write: 2 31
func__adjustHeap_afterLoop:
	li $2 0
	move $31 $111
	jr $ra
	move $31 $111
	jr $ra

# local: 31 4 114 119 121 122 123 23 126 127 38 129 130 131 133 134 128 132 136 137 139 140 142 143 138 141 146 147 145 151 152 154 155 150 30 153 157 158 149 156 159
# localSaved: 116 118 112 111
# global: 125
# Save in address:
# times: $31: 3  $111: 3  $4: 1  $112: 4  $114: 2  $116: 8  $118: 12  $119: 2  $121: 2  $122: 2  $123: 2  $23: 8  $38: 16  $126: 2  $127: 2  $129: 2  $130: 3  $128: 2  $30: 10  $131: 2  $133: 2  $134: 3  $132: 2  $125: 3  $136: 2  $137: 2  $139: 2  $140: 3  $138: 2  $142: 2  $143: 3  $141: 2  $146: 2  $147: 3  $145: 2  $149: 2  $151: 2  $152: 3  $150: 2  $154: 2  $155: 3  $153: 3  $157: 2  $158: 3  $156: 3  $159: 2  $2: 1 
# Read: 31 23 165 35
# Write: 161 163 165 35
func__heapSort:
	move $161 $31
	li $163 0
	li $165 0
	lw $35 0($23)
	bge $165 $35 func__heapSort_afterLoop

# Read: 23 38 167 35 165 168 169 171 172 170 30 173 174 175 177 178 163 176 179 180
# Write: 38 167 30 163 35 168 169 171 172 170 173 174 175 177 178 176 179 180 4
func__heapSort_loop:
	lw $38 4($23)
	lw $167 0($38)
	la $30 0($38)
	move $163 $167
	lw $38 4($23)
	lw $35 0($23)
	sub $168 $35 $165
	sub $169 $168 1
	mul $171 $169 4
	add $172 $38 $171
	lw $170 0($172)
	la $30 0($172)
	lw $38 4($23)
	lw $173 0($38)
	la $30 0($38)
	move $173 $170
	sw $173 0($30)
	lw $38 4($23)
	lw $35 0($23)
	sub $174 $35 $165
	sub $175 $174 1
	mul $177 $175 4
	add $178 $38 $177
	lw $176 0($178)
	la $30 0($178)
	move $176 $163
	sw $176 0($30)
	lw $35 0($23)
	sub $179 $35 $165
	sub $180 $179 1
	move $4 $180
	jal func__adjustHeap

# Read: 165 181 23 35
# Write: 181 165 35
func__heapSort_loopTail:
	add $181 $165 1
	move $165 $181
	lw $35 0($23)
	blt $165 $35 func__heapSort_loop

# Read: 161
# Write: 2 31
func__heapSort_afterLoop:
	li $2 0
	move $31 $161
	jr $ra
	move $31 $161
	jr $ra

# local: 31 23 38 167 168 169 171 172 170 30 173 174 175 177 178 176 179 180 181
# localSaved: 35 163
# global: 165 161
# Save in address:
# times: $31: 3  $161: 3  $163: 3  $165: 8  $23: 9  $35: 10  $38: 10  $167: 2  $30: 6  $168: 2  $169: 2  $171: 2  $172: 3  $170: 2  $173: 3  $174: 2  $175: 2  $177: 2  $178: 3  $176: 3  $179: 2  $180: 2  $4: 1  $181: 2  $2: 1 
# local: 2 31 23 33 34 36 37 38 41 44 45 30 43 46 47 49 52 53 51 54 55 56 4 5 59 62 63 61 60 67 68 70 71 66 69 73 74 65 72 58 76 77 85 86 87 90 91 93 94 95 97 98 92 96 100 101 103 104 106 107 102 105 109 114 119 121 122 123 126 127 129 130 131 133 134 128 132 136 137 139 140 142 143 138 141 146 147 145 151 152 154 155 150 153 157 158 149 156 159 167 168 169 171 172 170 173 174 175 177 178 176 179 180 181
# localSaved: 35 81 116 118 112 111 163
# global: 40 79 89 75 125 165 161
# Save in address: 32
# times: $4: 12  $2: 25  $23: 41  $31: 14  $32: 3  $33: 2  $34: 3  $30: 31  $35: 17  $36: 2  $37: 3  $40: 13  $38: 54  $41: 2  $44: 2  $45: 3  $43: 3  $46: 2  $47: 2  $49: 2  $52: 2  $53: 3  $51: 2  $5: 3  $54: 2  $55: 2  $56: 2  $58: 2  $59: 3  $60: 3  $62: 2  $63: 3  $61: 2  $65: 2  $67: 2  $68: 3  $66: 2  $70: 2  $71: 3  $69: 3  $73: 2  $74: 3  $72: 3  $75: 3  $76: 2  $77: 2  $79: 12  $81: 6  $83: 1  $85: 2  $86: 2  $87: 2  $90: 2  $91: 2  $93: 2  $94: 3  $92: 2  $95: 2  $97: 2  $98: 3  $96: 2  $89: 3  $100: 2  $101: 2  $103: 2  $104: 3  $102: 2  $106: 2  $107: 3  $105: 2  $109: 2  $111: 3  $112: 4  $114: 2  $116: 8  $118: 12  $119: 2  $121: 2  $122: 2  $123: 2  $126: 2  $127: 2  $129: 2  $130: 3  $128: 2  $131: 2  $133: 2  $134: 3  $132: 2  $125: 3  $136: 2  $137: 2  $139: 2  $140: 3  $138: 2  $142: 2  $143: 3  $141: 2  $146: 2  $147: 3  $145: 2  $149: 2  $151: 2  $152: 3  $150: 2  $154: 2  $155: 3  $153: 3  $157: 2  $158: 3  $156: 3  $159: 2  $161: 3  $163: 3  $165: 8  $167: 2  $168: 2  $169: 2  $171: 2  $172: 3  $170: 2  $173: 3  $174: 2  $175: 2  $177: 2  $178: 3  $176: 3  $179: 2  $180: 2  $181: 2 
